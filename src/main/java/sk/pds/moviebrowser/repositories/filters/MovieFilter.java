package sk.pds.moviebrowser.repositories.filters;

import org.thymeleaf.util.StringUtils;
import sk.pds.moviebrowser.models.filtersModels.MovieFilterModel;

public class MovieFilter extends FilterBuilder {

    public MovieFilter(MovieFilterModel filter) {
        var multiplier = 1;

        if (filter.getRatingMax() == null && filter.getRatingMin() == null && (StringUtils.isEmpty(filter.getOrderBy()) || !filter.getOrderBy().contains("RATING"))) {
            super.groupBy = "group by M.ID_MOVIE, M.ORIGINAL_TITLE, M.TITLE, M.ID_IMDB, M.ID_TMDB, M.BUDGET, M.POPULARITY, M.RELEASE_DATE, M.RUNTIME, M.ORIGINAL_LANGUAGE";
        } else {
            super.groupBy = "group by M.ID_MOVIE";
        }

        if (!StringUtils.isEmpty(filter.getTitle())) {
            super.statementsWhere.add("(M.TITLE like ? OR M.ORIGINAL_TITLE like ?)");
            super.parameters.add("%" + filter.getTitle().toLowerCase() + "%");
            super.parameters.add("%" + filter.getTitle().toLowerCase() + "%");
        }

        if (!StringUtils.isEmpty(filter.getIdImdb())) {
            super.statementsWhere.add("M.ID_IMDB = ?");
            super.parameters.add(this.removePrefixFomImdbId(filter.getIdImdb().toLowerCase()));
        }

        if (filter.getIdTmdb() != null) {
            super.statementsWhere.add("M.ID_TMDB = ?");
            super.parameters.add(filter.getIdTmdb());
        }

        if (filter.getBudgetMin() != null) {
            super.statementsWhere.add("M.BUDGET >= ?");
            super.parameters.add(filter.getBudgetMin());
        }

        if (filter.getBudgetMax() != null) {
            super.statementsWhere.add("M.BUDGET <= ?");
            super.parameters.add(filter.getBudgetMax());
        }

        if (filter.getAdult() != null) {
            super.statementsWhere.add("M.ADULT = ?");
            super.parameters.add(filter.getAdult() ? 1 : 0);
        }

        if (!StringUtils.isEmpty(filter.getOriginalLanguage())) {
            super.statementsWhere.add("LOWER(M.ORIGINAL_LANGUAGE) like ?");
            super.parameters.add(filter.getOriginalLanguage().toLowerCase());
        }

        if (filter.getReleaseDateTop() != null) {
            super.statementsWhere.add("M.RELEASE_DATE <= ?");
            super.parameters.add(filter.getReleaseDateTop());
        }

        if (filter.getReleaseDateBottom() != null) {
            super.statementsWhere.add("M.RELEASE_DATE >= ?");
            super.parameters.add(filter.getReleaseDateBottom());
        }

        if (!StringUtils.isEmpty(filter.getCompanies())) {
            if (filter.getCompanies().contains(",")) {
                multiplier = multiplier * filter.getCompanies().split(",").length;
                filter.setCompanies("'" + filter.getCompanies().replace(",", "','").toLowerCase() + "'");
                super.statementsWhere.add("LOWER(C2.NAME IN) (" + filter.getCompanies() + ")");
            } else {
                super.statementsWhere.add("LOWER(C2.NAME) LIKE ?");
                super.parameters.add(filter.getCompanies().toLowerCase());
            }
            super.joins.add("JOIN MOVIE_COMPANIES MC on M.ID_MOVIE = MC.ID_MOVIE JOIN COMPANIES C2 on C2.ID = MC.ID_COMPANY");
        }

        if (!StringUtils.isEmpty(filter.getKeywords())) {
            if (filter.getKeywords().contains(",")) {
                multiplier = multiplier * filter.getKeywords().split(",").length;
                filter.setKeywords("'" + filter.getKeywords().replace(",", "','").toLowerCase() + "'");
                super.statementsWhere.add("LOWER(K.NAME) IN (" + filter.getKeywords() + ")");
            } else {
                super.statementsWhere.add("LOWER(K.NAME) LIKE ?");
                super.parameters.add(filter.getKeywords().toLowerCase());
            }

            super.joins.add("JOIN MOVIE_KEYWORDS MK on M.ID_MOVIE = MK.ID_MOVIE JOIN KEYWORDS K on MK.ID_KEYWORD = K.ID_KEYWORD");
        }

        if (!StringUtils.isEmpty(filter.getGenres())) {
            if (filter.getGenres().contains(",")) {
                multiplier = multiplier * filter.getGenres().split(",").length;
                filter.setGenres("'" + filter.getGenres().replace(",", "','").toLowerCase() + "'");
                super.statementsWhere.add("LOWER(G.NAME) IN (" + filter.getGenres() + ")");
            } else {
                super.statementsWhere.add("LOWER(G.NAME) LIKE ?");
                super.parameters.add(filter.getGenres().toLowerCase());
            }
            super.joins.add("JOIN MOVIE_GENRES MG on M.ID_MOVIE = MG.ID_MOVIE JOIN GENRES G on MG.ID_GENRE = G.ID");
        }

        if (!StringUtils.isEmpty(filter.getCountries())) {
            if (filter.getCountries().contains(",")) {
                multiplier = multiplier * filter.getCountries().split(",").length;
                filter.setCountries("'" + filter.getCountries().replace(",", "','").toLowerCase() + "'");
                super.statementsWhere.add("(LOWER(PC.NAME) IN (" + filter.getCountries() + ") OR LOWER(PC.ISO_3166_1) IN (" + filter.getCountries() + "))");
            } else {
                super.statementsWhere.add("(LOWER(PC.NAME) LIKE ? OR LOWER(PC.ISO_3166_1) LIKE ?)");
                super.parameters.add(filter.getCountries().toLowerCase());
                super.parameters.add(filter.getCountries().toLowerCase());
            }

            super.joins.add("JOIN MOVIE_PRODUCTION_COUNTRIES MPC on M.ID_MOVIE = MPC.ID_MOVIE JOIN PRODUCTION_COUNTRIES PC on MPC.ID_PRODUCTION_COUNTRY = PC.ISO_3166_1");
        }

        if (!StringUtils.isEmpty(filter.getActors())) {
            if (filter.getActors().contains(",")) {
                multiplier = multiplier * filter.getActors().split(",").length;
                filter.setActors("'" + filter.getActors().replace(",", "','").toLowerCase() + "'");
                super.statementsWhere.add("LOWER(A2.NAME) IN (" + filter.getActors() + ")");
                super.joins.add("JOIN (SELECT MA.ID_MOVIE, MA.ID_ACTOR FROM MOVIE_ACTOR MA GROUP BY MA.ID_MOVIE, MA.ID_ACTOR) MA ON MA.ID_MOVIE = M.ID_MOVIE " +
                        "JOIN ACTOR A2 on A2.ID = MA.ID_ACTOR");
            } else {
                super.statementsWhere.add("LOWER(A2.NAME) LIKE ?");
                super.joins.add("JOIN MOVIE_ACTOR MA on M.ID_MOVIE = MA.ID_MOVIE JOIN ACTOR A2 on MA.ID_ACTOR = A2.ID");
                parameters.add(filter.getActors().toLowerCase());
            }

        }

        if (!StringUtils.isEmpty(filter.getCrew())) {
            if (filter.getCrew().contains(",")) {
                multiplier = multiplier * filter.getCrew().split(",").length;
                filter.setCrew("'" + filter.getCrew().replace(",", "','").toLowerCase() + "'");
                super.statementsWhere.add("LOWER(CM.NAME) IN (" + filter.getCrew() + ")");
                super.joins.add("JOIN (SELECT MC.ID_MOVIE, MC.ID_CREW FROM MOVIE_CREW MC group by MC.ID_MOVIE, MC.ID_CREW) MC on MC.ID_MOVIE = M.ID_MOVIE " +
                        "JOIN CREW_MEMBER CM ON MC.ID_CREW = CM.ID");
            } else {
                super.statementsWhere.add("LOWER(CM.NAME) LIKE ?");
                super.joins.add("JOIN MOVIE_CREW MC2 on M.ID_MOVIE = MC2.ID_MOVIE JOIN CREW_MEMBER CM on MC2.ID_CREW = CM.ID");
                parameters.add(filter.getCrew().toLowerCase());
            }
        }

        if (filter.getRatingMin() != null && filter.getRatingMax() != null) {
            super.joins.add("LEFT JOIN RATING R2 on M.ID_MOVIE = R2.ID_MOVIE");
            super.statementsHaving.add("AVG(R2.rating) between " + filter.getRatingMin() / 2 + " AND " + filter.getRatingMax() / 2);
        } else if (filter.getRatingMin() != null) {
            super.statementsHaving.add("AVG(R2.rating) >= ?");
            super.joins.add("LEFT JOIN RATING R2 on M.ID_MOVIE = R2.ID_MOVIE");
            super.parameters.add(filter.getRatingMin() / 2);
        } else if (filter.getRatingMax() != null) {
            super.statementsHaving.add("AVG(R2.rating) <= ?");
            super.joins.add("LEFT JOIN RATING R2 on M.ID_MOVIE = R2.ID_MOVIE");
            super.parameters.add(filter.getRatingMax() / 2);
        } else if (!StringUtils.isEmpty(filter.getOrderBy()) && filter.getOrderBy().contains("RATING")) {
            super.joins.add("LEFT JOIN RATING R2 on M.ID_MOVIE = R2.ID_MOVIE");
        }

        if (multiplier > 1) {
            this.statementsHaving.add("COUNT(M.ID_MOVIE) >= ?");
            this.parameters.add(multiplier);
        }

    }

    private Long removePrefixFomImdbId(String imdbId) {
        return Long.parseLong(imdbId.substring(2));
    }
}
