package sk.pds.moviebrowser.repositories.filters;

import java.util.ArrayList;
import java.util.List;

public abstract class FilterBuilder {

    public final List<String> statementsWhere = new ArrayList<>();
    public final List<String> statementsHaving = new ArrayList<>();
    public final List<String> joins = new ArrayList<>();
    public final List<Object> parameters = new ArrayList<>();
    public String groupBy = "", orderBy = "";

    public String getFilterString() {
        return   " " + String.join(" ", joins) +
                (statementsWhere.isEmpty() ? "" : " WHERE ")
                + String.join(" AND ", statementsWhere)
                + " " + groupBy + " " + orderBy + " "
                + (statementsHaving.isEmpty() ? "" : " HAVING ")
                + String.join(" AND ", statementsHaving) + " ";
    }

    public Object[] getParameters() {
        return parameters.toArray();
    }
}
