package sk.pds.moviebrowser.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.thymeleaf.util.StringUtils;
import sk.pds.moviebrowser.models.*;
import sk.pds.moviebrowser.models.mappers.*;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Optional;

//...Actor, Crew, User
@Repository
@RequiredArgsConstructor
public class PersonRepository {
    private final JdbcOperations operations;

    public static final int ACTOR_PAGE_SIZE = 30;
    public static final int CREW_PAGE_SIZE = 30;
    public static final int USER_PAGE_SIZE = 30;


    public Collection<Actor> getActorsList(int pageNum, String search) {
        String smallBuilder = "";
        if(!StringUtils.isEmpty(search)) {
            smallBuilder = "where LOWER(A.NAME) like '%" + search.toLowerCase() + "%' ";
        }
        return this.operations.query("SELECT * FROM (SELECT A.*,  RANK() over (order by A.ID) r, G.name as gender_name FROM ACTOR A JOIN GENDER G on A.gender = G.id " +
                smallBuilder +
                "order by A.ID) where r >= ? and r < ? order by ID", new Object[]{(pageNum - 1) * ACTOR_PAGE_SIZE, pageNum * ACTOR_PAGE_SIZE}, new ActorMapper());
    }

    public Collection<User> getUsersList(int pageNum, String search) {
        String smallBuilder = "";
        if(!StringUtils.isEmpty(search)) {
            smallBuilder = MessageFormat.format(" WHERE LOWER(MU.NAME) like {0} OR LOWER(MU.SURNAME) like {0} or LOWER(MU.NICKNAME) like {0}", "'%" + search.toLowerCase() + "%'");
        }
        return this.operations.query("SELECT * FROM (SELECT MU.*, RANK() over (order by MU.ID_USER) r FROM MOVIE_USER MU" +
                smallBuilder +
                ") where r >= ? and r < ?", new Object[]{(pageNum - 1) * USER_PAGE_SIZE, pageNum * USER_PAGE_SIZE}, new UserMapper());
    }

    public Collection<CrewMember> getCrewMembersList(int pageNum, String search) {
        String smallBuilder = "";
        if(!StringUtils.isEmpty(search)) {
            smallBuilder = "where LOWER(CM.NAME) like '%" + search.toLowerCase() + "%' ";
        }
        return this.operations.query("SELECT * FROM (SELECT CM.*, RANK() over (order by CM.ID) r, G.name as gender_name FROM CREW_MEMBER CM JOIN GENDER G on CM.gender = G.id " +
                smallBuilder +
                "order by CM.ID) where r >= ? and r < ?", new Object[]{(pageNum - 1) * CREW_PAGE_SIZE, pageNum * CREW_PAGE_SIZE}, new CrewMemberMapper());
    }

    public ActorDetail getActorById(long id) {
        return this.operations.query("SELECT A.*, G.name as gender_name FROM ACTOR A " +
                "JOIN GENDER G on G.id = A.gender where A.ID = ?", new Object[]{id}, new ActorDetailMapper()).stream().findFirst().orElseThrow(NullPointerException::new);
    }

    public User getUserById(long id) {
        return this.operations.query("SELECT * FROM MOVIE_USER MU where MU.ID_USER = ?", new Object[]{id}, new UserMapper()).stream().findFirst().orElseThrow(NullPointerException::new);
    }

    public CrewMemberDetail getCrewMemberById(long id) {
        return this.operations.query("SELECT CM.*, G.name as gender_name FROM CREW_MEMBER CM JOIN GENDER G on G.id = CM.gender where CM.ID = ?", new Object[]{id}, new CrewMemberDetailMapper()).stream().findFirst().orElseThrow(NullPointerException::new);
    }

    public Collection<ActorMovie> getActorsMovies(long idActor) {
        return this.operations.query("SELECT M.ID_MOVIE, MA.character, M.title FROM ACTOR A " +
                "JOIN MOVIE_ACTOR MA on MA.id_actor = A.id " +
                "JOIN MOVIE M on MA.id_movie = M.ID_MOVIE WHERE A.id = ?", new Object[]{idActor}, new ActorMovieMapper());
    }

    public Collection<UserRating> getUsersRatings(long idUser) {
        return this.operations.query("SELECT R.rating, R.rate_day, M.title, M.ID_MOVIE FROM  RATING R " +
                "JOIN MOVIE M on R.id_movie = M.id_movie where R.id_user = ?", new Object[]{idUser}, new UserRatingMapper());
    }

    public Collection<CrewMemberMovie> getCrewMembersMovies(long idCrew) {
        return this.operations.query("SELECT M.id_movie, M.title, J.job, d.department FROM MOVIE_CREW MC " +
                "JOIN MOVIE M on MC.ID_MOVIE = M.ID_MOVIE " +
                "JOIN DEPARTMENT D on MC.DEPARTMENT = D.ID " +
                "JOIN JOB J on MC.JOB = J.ID WHERE MC.ID_CREW = ?", new Object[]{idCrew}, new CrewMemberMovieMapper());
    }

    public String getActorByIdJson (long idActor) {
        String sql = "SELECT JSON_ObJECT( 'id' value A.ID, 'name' value A.Name, 'profile_path' value a.profile_path, 'gender' value a.gender, 'gender_name' value G.name, " +
                "'movies' value Json_arrayagg(JSON_OBJECT('id' value M.ID_MOVIE, 'character' value MA.character, 'title' value M.title) " +
                "returning clob) returning clob)" +
                "FROM ACTOR A " +
                "JOIN GENDER G on G.id = A.gender " +
                "JOIN MOVIE_ACTOR MA on MA.id_actor = A.id " +
                "JOIN MOVIE M on MA.id_movie = M.ID_MOVIE WHERE A.id = ? " +
                "Group by A.ID, A.Name,a.profile_path, a.gender, G.name";

        return this.operations.queryForObject(sql, new Object[]{idActor} ,String.class);
    }

    public int getActorsCount() {
        return Optional.ofNullable(this.operations.queryForObject("SELECT COUNT(ID) FROM ACTOR", Integer.class)).orElse(0);
    }

    public int getUsersCount() {
        return Optional.ofNullable(this.operations.queryForObject("SELECT COUNT(ID_USER) FROM MOVIE_USER", Integer.class)).orElse(0);
    }

    public int getCrewCount() {
        return Optional.ofNullable(this.operations.queryForObject("SELECT COUNT(ID) FROM CREW_MEMBER", Integer.class)).orElse(0);
    }

    public int register(String surname, String name, String password, String nickname) {
        if(Optional.ofNullable(this.operations.queryForObject("SELECT COUNT(ID_USER) FROM MOVIE_USER WHERE nickname = ?", Integer.class, nickname)).orElse(0) == 0) {
           return this.operations.update("INSERT INTO MOVIE_USER VALUES (movie_users_seq.nextval, ?, ?, ?, ?)", name, surname, nickname, password);
        }
        return 0;
    }

    public Long getIdByNickname(String username) {
        return this.operations.queryForObject("SELECT ID_USER FROM MOVIE_USER WHERE NICKNAME = ?", Long.class, username);
    }

}
