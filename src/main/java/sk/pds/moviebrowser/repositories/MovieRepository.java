package sk.pds.moviebrowser.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import org.thymeleaf.util.StringUtils;
import sk.pds.moviebrowser.models.*;
import sk.pds.moviebrowser.models.filtersModels.MovieFilterModel;
import sk.pds.moviebrowser.models.mappers.*;
import sk.pds.moviebrowser.repositories.filters.MovieFilter;

import java.util.Collection;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class MovieRepository {
    private final JdbcOperations operations;
    public static final int MOVIE_PAGE_SIZE = 30;

    public Collection<MovieList> getMoviesCollection(int pageNum, MovieFilterModel filterModel) {
        var filter = new MovieFilter(filterModel);
        /*(filterModel.getOrderBy().contains("RATING") ? "group by M.ID_MOVIE, M.ORIGINAL_TITLE, M.TITLE, M.ID_IMDB, M.ID_TMDB, M.BUDGET, M.POPULARITY, M.RELEASE_DATE, M.RUNTIME, M2.rating " +
                filterModel.getOrderBy() : filterModel.getOrderBy())*/
        if (filterModel.getRatingMax() == null && filterModel.getRatingMin() == null && (StringUtils.isEmpty(filterModel.getOrderBy()) || !filterModel.getOrderBy().contains("RATING"))) {
            return this.operations.query("SELECT M.ID_MOVIE, M.ORIGINAL_TITLE, M.TITLE, M.ID_IMDB, M.ID_TMDB, M.BUDGET, M.POPULARITY, M.RELEASE_DATE, M.RUNTIME, " +
                            "(SELECT COALESCE(AVG(R.RATING),0) FROM RATING R WHERE M.ID_MOVIE = R.ID_MOVIE GROUP BY R.ID_MOVIE) as rating FROM " +
                            "(SELECT M.ID_MOVIE, M.ORIGINAL_TITLE, M.TITLE, M.ID_IMDB, M.ID_TMDB, M.BUDGET, M.POPULARITY, M.RELEASE_DATE, M.RUNTIME FROM " +
                            "(SELECT M.ID_MOVIE, M.ORIGINAL_TITLE, M.TITLE, M.ID_IMDB, M.ID_TMDB, M.BUDGET, M.POPULARITY, M.RELEASE_DATE, M.RUNTIME, M.ORIGINAL_LANGUAGE, ROW_NUMBER() over (" + filterModel.getOrderBy() + ") r FROM MOVIE M" +
                            filter.getFilterString() + ") M WHERE " +
                            ("r between " + (pageNum - 1) * MovieRepository.MOVIE_PAGE_SIZE + " and " + pageNum * MovieRepository.MOVIE_PAGE_SIZE) + " ) M " + filterModel.getOrderBy(),
                    filter.getParameters(), new MovieListMapper());
        } else {
            return this.operations.query("SELECT * FROM (SELECT M.ID_MOVIE, M.ORIGINAL_TITLE, M.TITLE, M.ID_IMDB, M.ID_TMDB, M.BUDGET, M.POPULARITY, M.RELEASE_DATE, M.RUNTIME, M2.rating, ROW_NUMBER() over (order by M2.rating) r FROM MOVIE M " +
                            "JOIN (SELECT M.ID_MOVIE, COALESCE(AVG(RATING),0) as rating FROM MOVIE M " +
                            filter.getFilterString() + " ) M2 on M2.ID_MOVIE = M.ID_MOVIE " +
                            (filterModel.getOrderBy().contains("RATING") ? "group by M.ID_MOVIE, M.ORIGINAL_TITLE, M.TITLE, M.ID_IMDB, M.ID_TMDB, M.BUDGET, M.POPULARITY, M.RELEASE_DATE, M.RUNTIME, M2.rating " + filterModel.getOrderBy() : filterModel.getOrderBy()) + " ) M " +
                            ("where r between " + (pageNum - 1) * MovieRepository.MOVIE_PAGE_SIZE + " and " + pageNum * MovieRepository.MOVIE_PAGE_SIZE),
                    filter.getParameters(), new MovieListMapper());
        }

    }

    public MovieDetail getMovieById(long id) {
        return this.operations.query("SELECT MP.PICTURE, M.*,(SELECT COALESCE(AVG(R.RATING),0) " +
                "FROM RATING R WHERE M.ID_MOVIE = R.ID_MOVIE GROUP BY R.ID_MOVIE) as rating FROM MOVIE M " +
                "LEFT JOIN MOVIE_PICTURE_REMOTE@db_link_picture MP on MP.ID_MOVIE = M.ID_MOVIE " +
                "WHERE M.id_movie = ?", new Object[]{id}, new MovieDetailMapper())
                .stream().findFirst().orElseThrow(NullPointerException::new);
    }

    public Collection<MovieCrewMember> getMovieCrew(long idMovie) {
        return this.operations.query("SELECT CM.*, D.DEPARTMENT, J.JOB, MC.ID_CREDIT, G.name as gender_name FROM MOVIE M " +
                "JOIN MOVIE_CREW MC on M.id_movie = MC.ID_MOVIE " +
                "JOIN CREW_MEMBER CM on MC.ID_CREW = CM.ID " +
                "JOIN DEPARTMENT D on MC.DEPARTMENT = D.ID " +
                "JOIN JOB J on MC.JOB = J.ID " +
                "JOIN GENDER G on CM.gender = G.id " +
                "WHERE M.id_movie = ? ORDER BY D.DEPARTMENT DESC", new Object[]{idMovie}, new MovieCrewMemberMapper());
    }

    public Collection<MovieActor> getActors(long idMovie) {
        return this.operations.query("SELECT A.*, MA.ID_CREDIT, MA.ORDER_ACTOR, MA.ID_CAST, MA.CHARACTER, G.name as gender_name FROM MOVIE M " +
                "JOIN MOVIE_ACTOR MA on M.id_movie = MA.ID_MOVIE " +
                "JOIN ACTOR A on MA.id_actor = A.id " +
                "JOIN GENDER G on A.gender = G.id " +
                "WHERE M.id_movie = ? order by MA.ORDER_ACTOR", new Object[]{idMovie}, new MovieActorMapper());
    }

    public Collection<Company> getCompanies(long idMovie) {
        return this.operations.query("SELECT C2.name, C2.id FROM MOVIE M " +
                "JOIN MOVIE_COMPANIES MC on M.id_movie = MC.id_movie " +
                "JOIN COMPANIES C2 on MC.id_company = C2.id WHERE M.id_movie = ?", new Object[]{idMovie}, new CompanyMapper());
    }

    public Collection<Genre> getGenres(long idMovie) {
        return this.operations.query("SELECT G.id, G.name FROM MOVIE M " +
                "JOIN MOVIE_GENRES MG on M.id_movie = MG.id_movie " +
                "JOIN GENRES G on MG.id_genre = G.id WHERE M.id_movie = ?", new Object[]{idMovie}, new GenreMapper());
    }

    public Collection<SpokenLanguage> getSpokenLanguages(long idMovie) {
        return this.operations.query("SELECT SL.name, sl.iso_639_1 FROM MOVIE M " +
                "JOIN MOVIE_SPOKEN_LANGUAGES MSL on M.id_movie = MSL.id_movie " +
                "JOIN SPOKEN_LANGUAGES SL on MSL.id_spoken_language = SL.iso_639_1 WHERE M.id_movie = ?", new Object[]{idMovie}, new SpokenLanguageMapper());
    }

    public Collection<ProductionCountry> getProductionCountries(long idMovie) {
        return this.operations.query("SELECT PC.name, PC.iso_3166_1 FROM MOVIE M " +
                "JOIN MOVIE_PRODUCTION_COUNTRIES MPC on M.id_movie = MPC.id_movie " +
                "JOIN PRODUCTION_COUNTRIES PC on MPC.id_production_country = PC.iso_3166_1 WHERE M.id_movie = ?", new Object[]{idMovie}, new ProductionCountryMapper());
    }

    public Collection<Keyword> getKeywords(long idMovie) {
        return this.operations.query("SELECT K.name, K.id_keyword FROM MOVIE M " +
                "JOIN MOVIE_KEYWORDS MK on M.id_movie = MK.id_movie " +
                "JOIN KEYWORDS K on MK.id_keyword = K.id_keyword WHERE M.id_movie = ?", new Object[]{idMovie}, new KeywordMapper());
    }

    public int getMoviesCount() {
        return Optional.ofNullable(this.operations.queryForObject("SELECT COUNT(ID_MOVIE) FROM MOVIE", Integer.class)).orElse(0);
    }

    public int rateMovie(String nickname, String password, double rating, long idMovie) {
        var user = this.operations.query("SELECT * FROM MOVIE_USER WHERE NICKNAME = ?", new Object[]{nickname}, new UserMapper()).stream().findFirst().orElse(null);

        if (user != null && user.getPassword() != null && (new BCryptPasswordEncoder().matches(password, user.getPassword()))) {
            int count = Optional.ofNullable(this.operations.queryForObject("SELECT COUNT(ID_MOVIE) FROM RATING WHERE ID_MOVIE = ? AND ID_USER = ?", Integer.class, idMovie, user.getId())).orElse(0);
            if (count > 0) {
                return this.operations.update("UPDATE RATING SET RATING = ? WHERE ID_USER = ? AND ID_MOVIE = ?", rating, user.getId(), idMovie);
            } else {
                return this.operations.update("INSERT INTO RATING SELECT MU.ID_USER, ?, ?, sysdate FROM MOVIE_USER MU " +
                        "WHERE MU.ID_USER = ?", idMovie, rating, user.getId());
            }
        }

        return 0;
    }
}