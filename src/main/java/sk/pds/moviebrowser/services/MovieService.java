package sk.pds.moviebrowser.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import sk.pds.moviebrowser.models.MovieDetail;
import sk.pds.moviebrowser.models.MovieList;
import sk.pds.moviebrowser.models.filtersModels.MovieFilterModel;
import sk.pds.moviebrowser.repositories.MovieRepository;

import java.util.Collection;

@Service
@RequiredArgsConstructor
public class MovieService {

    private final MovieRepository movieRepository;

    public MovieDetail getMovieDetailById(long idMovie) {
        var movieDetail = this.movieRepository.getMovieById(idMovie);
        return movieDetail.setActors(this.movieRepository.getActors(idMovie))
                .setCompanies(this.movieRepository.getCompanies(idMovie))
                .setCrew(this.movieRepository.getMovieCrew(idMovie))
                .setGenres(this.movieRepository.getGenres(idMovie))
                .setKeywords(this.movieRepository.getKeywords(idMovie))
                .setProductionCountries(this.movieRepository.getProductionCountries(idMovie))
                .setSpokenLanguages(this.movieRepository.getSpokenLanguages(idMovie));
    }

    public Collection<MovieList> getMovies(int pageNum, MovieFilterModel movieFilterModel) {
        return this.movieRepository.getMoviesCollection(pageNum, movieFilterModel);
    }

    public int rateMovie(String nickname, String password, double rating, long idMovie) {
        return this.movieRepository.rateMovie(nickname, password, rating / 2, idMovie);
    }

    public int getMoviesCount() {
        return this.movieRepository.getMoviesCount();
    }
}
