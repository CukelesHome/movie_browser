package sk.pds.moviebrowser.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import sk.pds.moviebrowser.models.Actor;
import sk.pds.moviebrowser.models.ActorDetail;
import sk.pds.moviebrowser.models.CrewMember;
import sk.pds.moviebrowser.models.User;
import sk.pds.moviebrowser.repositories.PersonRepository;

import java.util.Collection;

@Service
@RequiredArgsConstructor
public class PersonService {

    private final PersonRepository personRepository;

    public Collection<Actor> getActorsList(int pageNum, String search) {
        return this.personRepository.getActorsList(pageNum, search);
    }

    public Collection<User> getUsersList(int pageNum, String search) {
        return this.personRepository.getUsersList(pageNum, search);
    }

    public Collection<CrewMember> getCrewMembersList(int pageNum, String search) {
        return this.personRepository.getCrewMembersList(pageNum, search);
    }

    public ActorDetail getActorById(long id) {
        return this.personRepository.getActorById(id)
                .setMovies(this.personRepository.getActorsMovies(id));
    }

    public User getUsersById(long id) {
        return this.personRepository.getUserById(id).setRatings(this.personRepository.getUsersRatings(id));
    }

    public CrewMember getCrewMembersById(long id) {
        return this.personRepository.getCrewMemberById(id).setMovies(this.personRepository.getCrewMembersMovies(id));
    }

    public int register(String surname, String name, String password, String nickname) {
        return this.personRepository.register(surname, name, new BCryptPasswordEncoder().encode(password), nickname);
    }

    public Long getIdByNickname(String username) {
        return this.personRepository.getIdByNickname(username);
    }

    public String getActorByIdJson(long id) {
        return this.personRepository.getActorByIdJson(id);
    }

    public int getActorsCount() {
        return this.personRepository.getActorsCount();
    }

    public int getUsersCount() {
        return this.personRepository.getUsersCount();
    }

    public int getCrewCount() {
        return this.personRepository.getCrewCount();
    }
}
