package sk.pds.moviebrowser.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import sk.pds.moviebrowser.repositories.PersonRepository;
import sk.pds.moviebrowser.services.PersonService;

@Controller
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
    private final PersonService personService;

    @GetMapping(value = "/{page}")
    public String users(Model model, @PathVariable int page, @RequestParam(name = "search", required = false) String search) {
        model.addAttribute("users", this.personService.getUsersList(page, search));
        model.addAttribute("template", "users");
        model.addAttribute("page", page);
        model.addAttribute("startPage", Math.max(1, page - 3));
        model.addAttribute("endPage", Math.min(this.personService.getUsersCount() / PersonRepository.USER_PAGE_SIZE + 1,
                page + 3));
        return "body";
    }

    @GetMapping(value = "detail/{id}")
    public String userDetail(Model model, @PathVariable long id) {
        model.addAttribute("user", this.personService.getUsersById(id));
        model.addAttribute("template", "user");
        if(!model.containsAttribute("inUse")) {
            model.addAttribute("inUse", false);
        }
        return "body";
    }

    @GetMapping(value = "register/page")
    public String registerPage(Model model) {
        model.addAttribute("template", "register");
        return "body";
    }

    @PostMapping(value = "register")
    public String register(Model model, @RequestParam String name, @RequestParam String surname,
                           @RequestParam String password, @RequestParam String nickname) {
        var updated = this.personService.register(surname, name, password, nickname);
        if(updated == 1) {
            return this.users(model, 1, nickname);
        } else {
            model.addAttribute("inUse", true);
            return registerPage(model);
        }
    }
}
