package sk.pds.moviebrowser.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import sk.pds.moviebrowser.repositories.PersonRepository;
import sk.pds.moviebrowser.services.PersonService;

@Controller
@RequestMapping("/actors")
@RequiredArgsConstructor
public class ActorController {

    private final PersonService personService;

    @GetMapping(value = "/{page}")
    public String actors(Model model, @PathVariable int page, @RequestParam(name = "search", required = false) String search) {
        model.addAttribute("actors", this.personService.getActorsList(page, search));
        model.addAttribute("template", "actors");
        model.addAttribute("page", page);
        model.addAttribute("startPage", Math.max(1, page - 3));
        model.addAttribute("endPage", Math.min(this.personService.getActorsCount() / PersonRepository.ACTOR_PAGE_SIZE + 1,
                page + 3));
        return "body";
    }

    @GetMapping(value = "detail/{id}")
    public String actorDetail(Model model, @PathVariable long id) {
        model.addAttribute("actor", this.personService.getActorById(id));
        model.addAttribute("template", "actor");
        return "body";
    }
}
