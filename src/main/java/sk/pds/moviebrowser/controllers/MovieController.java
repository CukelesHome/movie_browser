package sk.pds.moviebrowser.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import sk.pds.moviebrowser.models.filtersModels.MovieFilterModel;
import sk.pds.moviebrowser.repositories.MovieRepository;
import sk.pds.moviebrowser.services.MovieService;
import sk.pds.moviebrowser.services.PersonService;

import java.time.LocalDate;

@Controller
@RequestMapping("/movies")
@RequiredArgsConstructor
public class MovieController {

    private final MovieService movieService;
    private final UserController userController;
    private final PersonService personService;

    @RequestMapping(value = "/{page}", method = RequestMethod.GET)
    public String movies(Model model, @PathVariable int page, @RequestParam(name = "title", required = false) String title,
                         @RequestParam(name = "idImdb", required = false) String idImdb, @RequestParam(name = "idTmdb", required = false) Long idTmdb,
                         @RequestParam(name = "budgetMin", required = false) Integer budgetMin, @RequestParam(name = "budgetMax", required = false) Integer budgetMax,
                         @RequestParam(name = "adult", required = false) Boolean adult, @RequestParam(name = "originalLanguage", required = false) String originalLanguage,
                         @RequestParam(name = "ratingMin", required = false) Double ratingMin, @RequestParam(name = "ratingMax", required = false) Double ratingMax,
                         @RequestParam(name = "releaseDateBottom", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate releaseDateBottom,
                         @RequestParam(name = "releaseDateTop", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate releaseDateTop,
                         @RequestParam(name = "companies", required = false) String companies, @RequestParam(name = "keywords", required = false) String keywords,
                         @RequestParam(name = "genres", required = false) String genres, @RequestParam(name = "countries", required = false) String countries,
                         @RequestParam(name = "actors", required = false) String actors, @RequestParam(name = "crew", required = false) String crew,
                         @RequestParam(name = "orderBy", required = false) String orderBy, @RequestParam(name = "asc-desc", required = false) String ascDesc) {

        var movies = this.movieService.getMovies(page, new MovieFilterModel(title, idImdb, idTmdb, budgetMin, budgetMax, adult, originalLanguage, ratingMin,
                ratingMax, releaseDateBottom, releaseDateTop, companies, keywords, genres, countries, actors, crew, orderBy, ascDesc));
        model.addAttribute("movies", movies);
        model.addAttribute("template", "movies");
        model.addAttribute("page", page);
        model.addAttribute("startPage", Math.max(1, page - 3));
        model.addAttribute("endPage", Math.min(this.movieService.getMoviesCount() / MovieRepository.MOVIE_PAGE_SIZE + 1,
                page + 3));
        if(!model.containsAttribute("wrongPassword")) {
            model.addAttribute("wrongPassword", false);
        }
        return "body";
    }

    @GetMapping(value = "detail/{id}")
    public String movieDetail(Model model, @PathVariable long id) {
        model.addAttribute("movie", this.movieService.getMovieDetailById(id));
        model.addAttribute("template", "movie");
        return "body";
    }

    @PostMapping(value = "rate/{id}")
    public String rateMovie(Model model, @PathVariable long id, String nickname, String password, Double rating) {
        var updated = this.movieService.rateMovie(nickname, password, rating, id);
        if(updated == 0) {
            model.addAttribute("wrongPassword", true);
        } else {
            return this.userController.userDetail(model, this.personService.getIdByNickname(nickname));
        }
        return movieDetail(model, id);
    }
}
