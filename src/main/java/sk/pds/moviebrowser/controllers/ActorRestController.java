package sk.pds.moviebrowser.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sk.pds.moviebrowser.services.PersonService;


@RestController
@RequestMapping(value = "/actor-rest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor

public class ActorRestController {
    private final PersonService personService;

    @GetMapping("/{id}")
    public String getString(@PathVariable long id)
    {
        return personService.getActorByIdJson(id);
    }
}