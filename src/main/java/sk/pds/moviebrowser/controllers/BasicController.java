package sk.pds.moviebrowser.controllers;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class BasicController {

    @GetMapping("/")
    public RedirectView redirectWithUsingRedirectView() {
        return new RedirectView("movies/1");
    }

    @GetMapping("/error")
    public RedirectView error() {
        return new RedirectView("movies/1");
    }
}
