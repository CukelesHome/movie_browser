package sk.pds.moviebrowser.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import sk.pds.moviebrowser.repositories.PersonRepository;
import sk.pds.moviebrowser.services.PersonService;

@Controller
@RequestMapping("/crew")
@RequiredArgsConstructor
public class CrewMemberController {
    private final PersonService personService;

    @GetMapping(value = "/{page}")
    public String crew(Model model, @PathVariable int page, @RequestParam(name = "search", required = false) String search) {
        model.addAttribute("crew", this.personService.getCrewMembersList(page, search));
        model.addAttribute("template", "crew");
        model.addAttribute("page", page);
        model.addAttribute("startPage", Math.max(1, page - 3));
        model.addAttribute("endPage", Math.min(this.personService.getCrewCount() / PersonRepository.CREW_PAGE_SIZE + 1,
                page + 3));
        return "body";
    }

    @GetMapping(value = "detail/{id}")
    public String crewDetail(Model model, @PathVariable long id) {
        model.addAttribute("crewDetail", this.personService.getCrewMembersById(id));
        model.addAttribute("template", "crewDetail");
        return "body";
    }
}
