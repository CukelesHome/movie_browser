package sk.pds.moviebrowser.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class MovieDetail extends Movie {
    private Collection<Company> companies;
    private Collection<Keyword> keywords;
    private Collection<Genre> genres;
    private Collection<ProductionCountry> productionCountries;
    private Collection<SpokenLanguage> spokenLanguages;
    private Collection<MovieActor> actors;
    private Collection<MovieCrewMember> crew;
    //private Collection<UserRating> userRatings;
}
