package sk.pds.moviebrowser.models.mappers;

import org.springframework.jdbc.core.RowMapper;
import sk.pds.moviebrowser.models.MovieActor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MovieActorMapper implements RowMapper<MovieActor> {
    @Override
    public MovieActor mapRow(ResultSet rs, int rowNum) throws SQLException {
        var actor = new MovieActor();
        ActorMapper.mapRow(rs, rowNum, actor);
        return actor.setIdCredit(rs.getString("ID_CREDIT"))
                .setOrder(rs.getShort("ORDER_ACTOR"))
                .setIdCast(rs.getLong("ID_CAST"))
                .setCharacter(rs.getString("CHARACTER") == null ? "unknown" : rs.getString("CHARACTER"));

    }
}
