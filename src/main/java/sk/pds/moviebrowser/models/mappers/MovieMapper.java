package sk.pds.moviebrowser.models.mappers;

import oracle.jdbc.OracleStruct;
import org.springframework.jdbc.core.RowMapper;
import sk.pds.moviebrowser.models.Movie;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MovieMapper implements RowMapper<Movie> {
    @Override
    public Movie mapRow(ResultSet rs, int rowNum) throws SQLException {
        return MovieMapper.mapRow(rs, rowNum, new Movie());
    }

    public static Movie mapRow(ResultSet rs, int rowNum, Movie movie) throws SQLException {
        MovieListMapper.mapRow(rs, rowNum, movie);

        var struct = (OracleStruct) rs.getObject("COLLECTION_BELONGINGS");

        return movie
                .setCollectionBelongings(struct.getAttributes())
                .setAdult(rs.getBoolean("ADULT"))
                .setHomePage(rs.getString("HOMEPAGE") == null ? "" : rs.getString("HOMEPAGE"))
                .setOverview(rs.getString("OVERVIEW"))
                .setRevenue(rs.getLong("REVENUE"))
                .setTagLine(rs.getString("TAGLINE"))
                .setOriginalLanguage(rs.getString("ORIGINAL_LANGUAGE"))
                .setVideo(rs.getBoolean("VIDEO"));
    }


}
