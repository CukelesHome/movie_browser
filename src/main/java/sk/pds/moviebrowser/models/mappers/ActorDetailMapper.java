package sk.pds.moviebrowser.models.mappers;

import org.springframework.jdbc.core.RowMapper;
import sk.pds.moviebrowser.models.ActorDetail;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ActorDetailMapper implements RowMapper<ActorDetail> {
    @Override
    public ActorDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
        var actor = new ActorDetail();
        ActorMapper.mapRow(rs, rowNum, actor);
        return actor;
    }
}
