package sk.pds.moviebrowser.models.mappers;

import org.springframework.jdbc.core.RowMapper;
import sk.pds.moviebrowser.models.UserRating;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRatingMapper implements RowMapper<UserRating> {
    @Override
    public UserRating mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new UserRating(rs.getDouble("RATING") * 2, rs.getDate("RATE_DAY"), rs.getString("TITLE"), rs.getLong("ID_MOVIE"));
    }
}
