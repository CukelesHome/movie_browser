package sk.pds.moviebrowser.models.mappers;

import org.springframework.jdbc.core.RowMapper;
import sk.pds.moviebrowser.models.MovieCrewMember;

import java.sql.ResultSet;
import java.sql.SQLException;


public class MovieCrewMemberMapper implements RowMapper<MovieCrewMember> {
    @Override
    public MovieCrewMember mapRow(ResultSet rs, int rowNum) throws SQLException {
        MovieCrewMember movieCrewMember = new MovieCrewMember();
        CrewMemberMapper.mapRow(rs, rowNum, movieCrewMember);
        return movieCrewMember
                .setIdCredit(rs.getString("ID_CREDIT"))
                .setJob(rs.getString("JOB"))
                .setDepartment(rs.getString("DEPARTMENT"));
    }
}
