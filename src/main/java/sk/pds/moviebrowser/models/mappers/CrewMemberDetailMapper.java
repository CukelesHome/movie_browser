package sk.pds.moviebrowser.models.mappers;

import org.springframework.jdbc.core.RowMapper;
import sk.pds.moviebrowser.models.CrewMember;
import sk.pds.moviebrowser.models.CrewMemberDetail;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CrewMemberDetailMapper implements RowMapper<CrewMemberDetail> {
    @Override
    public CrewMemberDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
        var crewMember = new CrewMemberDetail();
        CrewMemberMapper.mapRow(rs, rowNum, crewMember);
        return crewMember;
    }
}
