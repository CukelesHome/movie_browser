package sk.pds.moviebrowser.models.mappers;

import org.springframework.jdbc.core.RowMapper;
import sk.pds.moviebrowser.models.Keyword;

import java.sql.ResultSet;
import java.sql.SQLException;

public class KeywordMapper implements RowMapper<Keyword> {
    @Override
    public Keyword mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Keyword(rs.getLong("ID_KEYWORD"),
                rs.getString("NAME"));
    }
}
