package sk.pds.moviebrowser.models.mappers;

import org.springframework.jdbc.core.RowMapper;
import sk.pds.moviebrowser.models.MovieList;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MovieListMapper implements RowMapper<MovieList> {
    @Override
    public MovieList mapRow(ResultSet rs, int rowNum) throws SQLException {
        return mapRow(rs, rowNum, new MovieList());
    }

    public static MovieList mapRow(ResultSet rs, int rowNum, MovieList movieList) throws SQLException {
        return movieList
                .setId(rs.getLong("ID_MOVIE"))
                .setIdImdb(setImdbId(rs.getLong("ID_IMDB")))
                .setIdTmdb(rs.getLong("ID_TMDB"))
                .setBudget(rs.getLong("BUDGET"))
                .setOriginalTitle(rs.getString("ORIGINAL_TITLE"))
                .setPopularity((int)(rs.getDouble("POPULARITY") * 1000000))
                .setRating(Math.round(rs.getDouble("rating") * 200.0) / 100.0)
                .setReleaseDate(rs.getDate("RELEASE_DATE"))
                .setRunTime(rs.getShort("RUNTIME"))
                .setTitle(rs.getString("TITLE"));
    }

    private static String setImdbId(Long imdbId) {
        var raw = String.valueOf(imdbId);
        StringBuilder imdbIdString = new StringBuilder(raw);
        for(var i = 0; i < 7 - raw.length(); i++) {
            imdbIdString.insert(0, "0");
        }

        return "tt" + imdbIdString;
    }
}
