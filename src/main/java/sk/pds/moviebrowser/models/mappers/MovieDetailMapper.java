package sk.pds.moviebrowser.models.mappers;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.jdbc.core.RowMapper;
import sk.pds.moviebrowser.models.MovieDetail;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MovieDetailMapper implements RowMapper<MovieDetail> {

    @Override
    public MovieDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
        var movie = new MovieDetail();
        var blob = rs.getBlob("PICTURE");
        movie.setImage(Base64.encodeBase64String(blob.getBytes(1, ((int) blob.length()))));
        MovieMapper.mapRow(rs, rowNum, movie);
        return movie;
    }
}
