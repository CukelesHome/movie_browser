package sk.pds.moviebrowser.models.mappers;

import org.springframework.jdbc.core.RowMapper;
import sk.pds.moviebrowser.models.ActorMovie;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ActorMovieMapper implements RowMapper<ActorMovie> {

    @Override
    public ActorMovie mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new ActorMovie(rs.getLong("ID_MOVIE"), rs.getString("CHARACTER"), rs.getString("TITLE"));
    }
}
