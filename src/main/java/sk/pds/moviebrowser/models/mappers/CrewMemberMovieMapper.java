package sk.pds.moviebrowser.models.mappers;

import org.springframework.jdbc.core.RowMapper;
import sk.pds.moviebrowser.models.CrewMemberMovie;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CrewMemberMovieMapper implements RowMapper<CrewMemberMovie> {

    @Override
    public CrewMemberMovie mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new CrewMemberMovie(rs.getLong("ID_MOVIE"), rs.getString("DEPARTMENT"), rs.getString("JOB"),
                rs.getString("TITLE"));
    }
}
