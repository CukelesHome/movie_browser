package sk.pds.moviebrowser.models.mappers;

import org.springframework.jdbc.core.RowMapper;
import sk.pds.moviebrowser.models.SpokenLanguage;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SpokenLanguageMapper implements RowMapper<SpokenLanguage> {
    @Override
    public SpokenLanguage mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new SpokenLanguage(rs.getString("ISO_639_1"),
                rs.getString("NAME"));
    }
}
