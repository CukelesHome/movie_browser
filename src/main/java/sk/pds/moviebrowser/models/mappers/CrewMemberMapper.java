package sk.pds.moviebrowser.models.mappers;

import org.springframework.jdbc.core.RowMapper;
import sk.pds.moviebrowser.models.CrewMember;
import sk.pds.moviebrowser.models.MovieCrewMember;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CrewMemberMapper implements RowMapper<CrewMember> {
    @Override
    public CrewMember mapRow(ResultSet rs, int rowNum) throws SQLException {
        return CrewMemberMapper.mapRow(rs, rowNum, new CrewMember());
    }

    public static CrewMember mapRow(ResultSet rs, int rowNum, CrewMember crewMember) throws SQLException {
        return crewMember
                .setId(rs.getLong("ID"))
                .setName(rs.getString("NAME"))
                .setProfilePath(rs.getString("PROFILE_PATH"))
                .setGender(rs.getString("gender_name"));
    }
}
