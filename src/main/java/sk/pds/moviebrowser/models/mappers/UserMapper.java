package sk.pds.moviebrowser.models.mappers;

import org.springframework.jdbc.core.RowMapper;
import sk.pds.moviebrowser.models.User;
import sk.pds.moviebrowser.models.UserRating;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        return UserMapper.mapRow(rs, rowNum, new User());
    }

    public static User mapRow(ResultSet rs, int rowNum, User user) throws SQLException {
        return user
                .setId(rs.getLong("ID_USER"))
                .setName(rs.getString("NAME"))
                .setSurname(rs.getString("SURNAME"))
                .setPassword(rs.getString("PASSWORD"))
                .setNickname(rs.getString("NICKNAME"));
    }
}