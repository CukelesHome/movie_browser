package sk.pds.moviebrowser.models.mappers;

import org.springframework.jdbc.core.RowMapper;
import sk.pds.moviebrowser.models.Actor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ActorMapper implements RowMapper<Actor> {
    @Override
    public Actor mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ActorMapper.mapRow(rs, rowNum, new Actor());
    }

    public static Actor mapRow(ResultSet rs, int rowNum, Actor actor) throws SQLException {
        return actor
                .setId(rs.getLong("ID"))
                .setName(rs.getString("NAME"))
                .setProfilePath(rs.getString("PROFILE_PATH"))
                .setGender(rs.getString("gender_name"));
    }
}
