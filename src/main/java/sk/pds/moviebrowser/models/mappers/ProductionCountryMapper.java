package sk.pds.moviebrowser.models.mappers;

import org.springframework.jdbc.core.RowMapper;
import sk.pds.moviebrowser.models.ProductionCountry;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductionCountryMapper implements RowMapper<ProductionCountry> {

    @Override
    public ProductionCountry mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new ProductionCountry(rs.getString("ISO_3166_1"),
                rs.getString("NAME"));
    }
}
