package sk.pds.moviebrowser.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.sql.Blob;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Movie extends MovieList {
    private boolean adult;
    private String homePage;
    private String originalLanguage;
    private String overview;
    private long revenue;
    private String tagLine;
    private boolean video;
    private Object[] collectionBelongings;
    private String image;
}