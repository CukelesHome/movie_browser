package sk.pds.moviebrowser.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class User {
    private long id;
    private String password;
    private String name;
    private String surname;
    private String nickname;
    private Collection<UserRating> ratings;
}
