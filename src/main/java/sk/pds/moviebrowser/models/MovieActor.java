package sk.pds.moviebrowser.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class  MovieActor extends Actor {
    private String idCredit;
    private short order;
    private long idCast;
    private String character;
}
