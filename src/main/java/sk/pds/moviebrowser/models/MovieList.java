package sk.pds.moviebrowser.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class MovieList {
    private long id;
    private String idImdb;
    private long idTmdb;
    private String title;
    private String originalTitle;
    private double rating;
    private long budget;
    private int popularity;
    private Date releaseDate;
    private short runTime;
}
