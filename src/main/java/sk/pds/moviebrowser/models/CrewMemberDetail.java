package sk.pds.moviebrowser.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Collection;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class CrewMemberDetail extends CrewMember {
    private Collection<CrewMemberMovie> movies;
}
