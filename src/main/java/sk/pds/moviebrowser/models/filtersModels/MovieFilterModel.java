package sk.pds.moviebrowser.models.filtersModels;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.thymeleaf.util.StringUtils;

import java.time.LocalDate;

@Data
@Accessors(chain = true)
@NoArgsConstructor
public class MovieFilterModel {
    private String title;
    private String idImdb;
    private Long idTmdb;
    private Integer budgetMin;
    private Integer budgetMax;
    private Boolean adult;
    private String originalLanguage;
    private Double ratingMin;
    private Double ratingMax;
    private LocalDate releaseDateBottom;
    private LocalDate releaseDateTop;
    private String companies;
    private String keywords;
    private String genres;
    private String countries;
    private String actors;
    private String crew;
    private String orderBy;

    public MovieFilterModel(String title, String idImdb, Long idTmdb, Integer budgetMin, Integer budgetMax, Boolean adult, String originalLanguage, Double ratingMin, Double ratingMax, LocalDate releaseDateBottom, LocalDate releaseDateTop, String companies, String keywords, String genres, String countries, String actors, String crew, String orderBy, String ascDesc) {
        this.title = title;
        this.idImdb = idImdb;
        this.idTmdb = idTmdb;
        this.budgetMin = budgetMin;
        this.budgetMax = budgetMax;
        this.adult = adult;
        this.originalLanguage = originalLanguage;
        this.ratingMin = ratingMin;
        this.ratingMax = ratingMax;
        this.releaseDateBottom = releaseDateBottom;
        this.releaseDateTop = releaseDateTop;
        this.companies = companies;
        this.keywords = keywords;
        this.genres = genres;
        this.countries = countries;
        this.actors = actors;
        this.crew = crew;
        this.orderBy = StringUtils.isEmpty(orderBy) ? " order by M.ID_MOVIE " : " order by " + orderBy;
        this.orderBy += ("on".equals(ascDesc) ? " DESC" : "");
    }
}
