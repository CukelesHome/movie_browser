package sk.pds.moviebrowser;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import sk.pds.moviebrowser.repositories.MovieRepository;
import sk.pds.moviebrowser.services.MovieService;

import javax.annotation.PostConstruct;

@SpringBootApplication
@RequiredArgsConstructor
public class MovieBrowserApplication {

	private final MovieService service;

	public static void main(String[] args) {
		SpringApplication.run(MovieBrowserApplication.class, args);
		//:)
	}
}
